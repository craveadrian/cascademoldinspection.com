	<div id="content">
		<div class="row services">
			<h1>Services</h1>

			<b>WE’RE HERE TO HELP</b>
			<h2>CERTIFIED MOLD INSPECTION SERVICES</h2>

			<!-- <h3>VISUAL</h3> -->
			<!-- <dl>
				<dt><h2>Visual Inspection</h2></dt>
				<dd>
					<img src="public/images/common/svc-img1.jpg">
					<p class="p-pad">A visual inspection begins on the exterior of the home. The roof, flashings, gutters, downspouts, chimneys, siding, doors and windows are all examined to identify problem areas that allow water intrusion to occur.
						When inspecting the living spaces of the home moisture meters, infrared cameras, and particle counters are used to identify areas where water intrusion has occurred and microbial growth maybe occurring. A written report that includes pictures, findings and recommendations are emailed to the homeowner following the inspection.</p>
				<span class="clearfix"></span></dd>
			</dl> -->

			<!-- <h3>SAMPLING</h3> -->
			<!-- <dl>
				<dt><h2>Air Sampling</h2></dt>
				<dd class="w-images">
					<img src="public/images/common/svc-img2.jpg">
					<p>If visible mold is present anywhere in the house an air sample can help determine the effect mold is having on the air you breath in the home by identifying what types and how much mold there is in the air. If visible mold is not present, an air sample can help determine the probability of microbial growth occurring in the home.</p>
				</dd>
			</dl> -->

			<!-- <dl >
				<dt><h2>Surface Sampling</h2></dt>
				<dd class="w-images">
					<img src="public/images/common/svc-img3.jpg">
					<p>When visible mold is present surface sampling can identify the type of mold.</p><p> When visible mold is not present surface sampling can be used to determine if there may be hidden and unforeseen mold.</p>
				</dd>
			</dl> -->
		<!-- <h3>REPORTS</h3> -->

			<!-- <dl>
				<dt><h2>Mold Protocol</h2></dt>
				<dd>
					<p>We offer a written report called a “Mold Protocol,” a written plan of how to remediate mold contamination. The protocol should be given to a certified mold remediation company to perform the remediation.</p>
				</dd>
			</dl>
 -->
			<!-- <h3>REMOVAL</h3> -->

			<!-- <dl>
				<dt><h2>Mold Remediation</h2></dt>
				<dd>
					<img src="public/images/common/svc-img4.png">
					<p class="p-pad">Mold Remediation is the process of removing mold contamination from a structure. Cascade Mold Inspection LLC conducts all biological remediation following guidelines established by the Institute of Inspection Cleaning and Restoration (IICRC). Their document entitled IICRC S520 Standard and Reference Guide for Professional Mold Remediation outlines work practices and equipment to be utilized during the remediation procedure. We also follow recommendations outlined in the US EPA: Mold Remediation in Schools and Commercial Buildings, Publication EPA 402-K-01-001. Following these rules and guidelines insures that mold contamination is removed effectively and efficiently. Leaving you with a cleaner and healthier home.
					</p>
				<span class="clearfix"></span></dd>
			</dl> -->

			<div id="services_div">
					<div id="Visual_Inspection" class="services_stuffs">
							<h2>Visual Inspection</h2>
							<div class="SS_Left">
										<img src="public/images/common/svc-img1.jpg" alt="Visual Inspection">
							</div>
							<div class="SS_Right">
								<p id="serv_par_1">A visual inspection begins on the exterior of the home. The roof, flashings, gutters, downspouts, chimneys, siding, doors and windows
									are all examined to identify problem areas that allow water intrusion to occur. When inspecting the living spaces of the home moisture meters,
									infrared cameras, and particle counters are used to identify areas where water intrusion has occurred and microbial growth maybe occurring.
									A written report that includes pictures, findings and recommendations are emailed to the homeowner following the inspection.
								</p>
							</div>
							<div class="clearfix clear"></div>
					</div>

					<div id="Air_Sampling" class="services_stuffs">
							<h2>Air Sampling</h2>
							<div class="SS_Left">
										<img src="public/images/common/svc-img2.jpg" alt="Air Sampling">
							</div>
							<div class="SS_Right">
								<p id="serv_par_2">If visible mold is present anywhere in the house an air sample can help determine the effect mold is having on the air you breath
									in the home by identifying what types and how much mold there is in the air. If visible mold is not present, an air sample can help
									determine the probability of microbial growth occurring in the home.
							 </p>
							</div>
							<div class="clearfix clear"></div>
					</div>

					<div id="Surface_Sampling" class="services_stuffs">
							<h2>Surface Sampling</h2>
							<div class="SS_Left">
										<img src="public/images/common/svc-img3.jpg" alt="Surface Sampling">
							</div>
							<div class="SS_Right">
								<p id="serv_par_3">When visible mold is present surface sampling can identify the type of mold.</p>
								<p id="serv_par_4"> When visible mold is not present surface sampling can be used to determine if there may be hidden and unforeseen mold.</p>
							</div>
							<div class="clearfix clear"></div>
					</div>

					<div id="Mold_Remediation" class="services_stuffs">
							<h2>Mold Remediation</h2>
							<div class="SS_Left">
										<img src="public/images/common/svc-img4.png" alt="Mold Remediation">
							</div>
							<div class="SS_Right">
								<p id="serv_par_5">Mold Remediation is the process of removing mold contamination from a structure. Cascade Mold Inspection LLC conducts all biological
									remediation following guidelines established by the Institute of Inspection Cleaning and Restoration (IICRC). Their document entitled IICRC S520
									Standard and Reference Guide for Professional Mold Remediation outlines work practices and equipment to be utilized during the remediation procedure.
									We also follow recommendations outlined in the US EPA: Mold Remediation in Schools and Commercial Buildings, Publication EPA 402-K-01-001.
									Following these rules and guidelines insures that mold contamination is removed effectively and efficiently. Leaving you with a cleaner and healthier home.
								</p>
							</div>
							<div class="clearfix clear"></div>
					</div>

			</div>


			<!-- <h3>CERTIFIED</h3> -->
			<img src="public/images/common/svc-img5.jpg" class="macro">
			<img src="public/images/common/svc-img6.jpg" class="cmi">
			<p>All inspections and sampling are performed in accordance with generally accepted standards of mold inspection, and sampling analysis. They also follow analytical methods recommended by the American Industrial Hygiene Association (AIHA) and the American Conference of Governmental Industrial Hygienists (ACGIH). Currently there are no EPA standards for airborne mold spores.</p>


		</div>
	</div>
