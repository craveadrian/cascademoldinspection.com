<div id="content">
	<div class="row about">
		<h1>About Us</h1><br>
		<h2>MISSION, EXPERIENCE &amp; VISION</h2>
		<img src="public/images/common/abt-img1.png">

		<h2>Our History</h2>
		<b>12 Years of Experience</b>
		<p>I have been in the mold remediation and water damage restoration industry since 2005. I began as a laborer and worked my way up to a crew lead, production manager, and eventually operations manager. As a labor and crew lead I learned what mold is and how to deal with it. As a manger I learned how to communicate and best serve the homeowner.</p>
		<p>I understand what both water and mold damage can do to a home and it’s occupants. Cascade Mold Inspection LLC was created to help homeowners protect their home and families from these two destructive forces.</p>

		<h2>The Mission</h2>
		<b>Serve the Homeowner</b><br>
		<img src="public/images/common/abt-img2.png">

		<p>We will serve the homeowner with the utmost respect and care. We will provide the information and data necessary to help the homeowner protect both home and family.</p>
		<p>Your home is your largest asset. Your family is your greatest blessing. We want to help you protect and care for both.</p>
</div>