	<div id="content">
		<div class="row">
			<h1>Testimonials</h1>
			
			<div class="reviews">
				<span class="star">&bigstar;&bigstar;&bigstar;&bigstar;&bigstar;</span>

				<p>“Luke was great! Very friendly and professional. His inspection was thorough and his sampling method is great. We’re renovating our entire home and he was easily able to work around us and our contractors with no complaints and issues at all. Luke explained each method of testing, and let us choose the way we wanted the sample taken. He was right on time and responded to my urgent request immediately even considering that I made it around 9pm. He had our report emailed to us before he even left our home, which included pictures of everything he did find. Would recommend to anyone concerned about mold in their homes.”</p>

				<b> - Claire S. in Marysville, WA</b>
			</div>

			<div class="reviews">
				<span class="star">&bigstar;&bigstar;&bigstar;&bigstar;&bigstar;</span>

				<p>“On time, thorough, not remotely patronizing. Sharp guy who knows his business. Certainly would hire him again, and i recommend him highly.”</p>

				<b> - Peter S. in Langley, WA</b>
			</div>
			
			<div class="reviews">
				<span class="star">&bigstar;&bigstar;&bigstar;&bigstar;&bigstar;</span>

				<p>“ Luke, the owner, was fantastic to work with. I sent a request via Home Advisor late on a Saturday night and within an hour I had an email stating he would call me Sunday morning. I could not believe how quick the response was and he was at my home on Monday to complete the work. We were doing home renovations and discovered mold behind the walls. He did a surface inspection and gave me a report of the mold present and future options. I would definitely work with Cascade Mold Inspections again if necessary.”</p>

				<b> - Rebecca W. in Anacortes, WA</b>
			</div>

			<div class="reviews">
				<span class="star">&bigstar;&bigstar;&bigstar;&bigstar;&bigstar;</span>

				<p>“Responsive, personable, professional”</p>

				<b> - Paul R. in Bellingham, WA</b>
			</div>

			<div class="reviews">
				<span class="star">&bigstar;&bigstar;&bigstar;&bigstar;&bigstar;</span>

				<p>“Luke is great to work with and we highly recommend Cascade Mold. They came over promptly, were very knowledgeable and answered (many) questions so that we understood what we were dealing with and the best ways to manage it.”</p>

				<b> - Suzanne M. in Anacortes, WA</b>
			</div>

			<div class="reviews">
				<span class="star">&bigstar;&bigstar;&bigstar;&bigstar;&bigstar;</span>

				<p>“I highly recommend Cascade Mold Inspection, Luke is very knowledgeable, professional and thorough if your at all concerned about mold definitely call him he does a great job.”</p>

				<b> - Tom B. in Mount Vernon, WA</b>
			</div>

			<div class="reviews">
				<span class="star">&bigstar;&bigstar;&bigstar;&bigstar;&bigstar;</span>

				<p>“5 stars. Reliable, supportive, effective, considerate, knowledgeable. Likable!”</p>

				<b> - Paige M. in East Sound, WA</b>
			</div>

			<div class="reviews">
				<span class="star">&bigstar;&bigstar;&bigstar;&bigstar;&bigstar;</span>

				<p>“Luke at Cascade Mold Inspection was prompt with communication, punctual, courteous, professional, and helpful and informative about our minor mold situation. I can definitely recommend Cascade Mold Inspection!”</p>

				<b> - Renee F. in Coupville, WA</b>
			</div>

		</div>
	</div>
